extern malloc
extern free
extern printf

section .data
extern malloc
msj db 'Si es el puntero'
len equ $ - msj

emptyNode db " {}",0
viewNode db " { %d", 0
closeNode db "}", 0


numeros dw 5,1,7,10,2,5 
tam db 24

tamanoArbol db 12

section .bss


section .text

global crearArbolB
global mostrar_abb

crearArbolB:

    push ebp ; guardo base point
    mov ebp, esp ; mi nuevo base point es stack point
    ;push ebx ;guardo el registro que voy a usar para el puntero
    
    mov edx, [ebp + 12];izquier
    mov ecx, [ebp + 16] ; derecho
    mov eax, [ebp + 8]; valor
    
    cmp edx, 0
    je agregarNodoIzq;pedir memoria para nuevo nodo y guardar valor

    cmp ecx, 0
    ;je agregarNodoDer
    ;ebx valor nodo
    ;ebx + 4 puntero nodo izq
    ;ebx + 8 puntero nodo der
    ;cmp edx, [ebx];comparo valor parametro con valor nodo
    JE volver
    ;JG ingresarDerecho;IF greater
    ;jmp ingresarIzquierdo


agregarNodoIzq:
        push eax
        push 12
        call malloc
        add esp, 4 ;borro el parametro para pedir memoria
        pop eax
  
        mov [edx], eax ;guardo valor
        mov ebx, 0
        mov [edx + 4], ebx;seteo nodo a cero
        mov [edx + 8], ebx;seteo nodo a cero
        mov [ebp + 4], edx

        jmp imprimir

agregarNodoDer:
        push ecx
        push 12
        call malloc
        add esp, 4 ;borro el parametro para pedir memoria
        pop ecx
  
        mov [eax], ecx ;guardo valor
        mov ebx, 0
        mov [eax + 4], ebx;seteo nodo a cero
        mov [eax + 8], ebx;seteo nodo a cero
        mov [ebp + 4], eax

        jmp imprimir

ingresarIzquierdo: 

        push edx; pusheo valor parametro
        mov eax, [edx];guardo nodo derecho para poder pushearlo
        push eax;pusheo nodo der
        call crearArbolB;devuelve el nodo en EAX
        mov eax, [ebx];puntero nodo actual
        mov [edx], eax;agrego nodo nuevo
         mov eax,ebx;hago esto para que no pise los nodos de arriba
        add esp, 8 ; desapilo
        jmp volver




ingresarDerecho:   
        cmp dword[ecx], 0
        jne agregarNodoDer

        mov dword[ecx], eax    ;pone el valor al nodo izquierdo
        jmp volver

mostrar_abb:
    push ebp ; guardo base point
    mov ebp, esp ; mi nuevo base point es stack point
    mov ebx, [ebp + 8];puntero nodo
    cmp ebx, 0 ;si es cero, es porque no hay nodo
    je mostrarVacio
    call mostrarInicioNodo
    push ebx;guardo nodo actual
    mov eax, [ebx +4];paso por parametro el nodo izq
    push eax
    call mostrar_abb
    add esp, 4;saco el push de nodo izq
    pop ebx;recupero nodo actual
    mov eax, [ebx +8];paso por parametro el nodo derecho
    push eax
    call mostrar_abb
    call mostrarFinNodo
    jmp volver


mostrarVacio:
    push eax
    push ebx
    push edx
    push emptyNode
    call printf
    add esp, 4
    pop edx
    pop ebx
    pop edx
    jmp volver


mostrarInicioNodo:
    push eax
    push ebx
    push edx
    mov eax, [ebx]
    push eax
    push viewNode
    call printf
    add esp, 8
    pop edx
    pop ebx
    pop edx
    ret

mostrarFinNodo:
    push eax
    push ebx
    push edx
    push closeNode
    call printf
    add esp, 4
    pop edx
    pop ebx
    pop edx
    ret




    volver:
    ;reinicio lo que apile
    ;pop ebx
    mov esp, ebp
    pop ebp
    ret



    
imprimir:
        mov eax, 4
        mov ebx, 1
        mov ecx, msj
        mov edx, len
        int 0x80
