#include <stdio.h>
#include <stdlib.h>

struct arbol
{
        int valor;
        struct arbol *izq;
        struct arbol *der;
};

struct arbol *buscarNodo(int valor);

struct arbol *crearArbolB(int valor, struct arbol *izq, struct arbol *der);

void mostrar_abb(struct arbol *arb);

int suma(int a, int b, int c);

int main(int argc, char *argv[])
{

        int valorInicial, tamanioArbol, contador, valorNuevo;

        struct arbol *arbolito;

        printf("Ingrese el tamaño del arbol: ");

        scanf("%d", &tamanioArbol);

        arbolito = (struct arbol *)malloc(tamanioArbol * sizeof(struct arbol));

        contador = tamanioArbol;


        printf("Ingrese un numero: ");

        scanf("%d", &valorInicial);



        arbolito->valor = valorInicial;



        while (contador != 1)
        {

                printf("Ingrese un numero: ");

                scanf("%d", &valorNuevo);

                crearArbolB(valorNuevo,arbolito->izq,arbolito->der);

                

                contador--;
        }

        struct arbol *punteroArbol;

        punteroArbol = arbolito;



        //while(num != 10){
        //printf("Ingrese un numero");
        //scanf("%d", &num);

        //crearArbolB(valorInicial,punteroArbol->izq,punteroArbol->der);

        printf("Arbol Binario:  \n");
        mostrar_abb(arbolito);

        //};

        //printf("valor arbol izquierdo %d :",(punteroArbol->izq)->valor);

        return 0;
}

/*

int i = 0 
valorAComparar = valorInicial
while(i<3){

    si el valor es menor al valor de nodo de la rama 
            entonces se debe cambiar la rama izquierda

    si el valor es mayor al valor de nodo de la rama 
            entonces se debe cambiar la rama derecha 


    i++
}



*/